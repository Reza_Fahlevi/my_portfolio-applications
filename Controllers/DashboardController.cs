﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Linq.Dynamic.Core;
using System.Diagnostics;
using My_API.Additional;
using My_API.Connection;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using My_API.Models;
using Newtonsoft.Json;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Net.Http.Headers;
using static My_API.Additional.DataTables;
using System.Net;
using static My_API.Additional.ResponseAPI.ResponseMasterMenu;

namespace My_Portfolio.Controllers
{
	public class DashboardController : Controller
    {
        #region Property DataTables
        [BindProperty]
        public DataTables.DataTablesRequest? DataTablesRequest { get; set; }
        #endregion

        #region Property Connection Database
        static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
        static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
        public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;
        public static string RoleHakAkses = AppSettingJSON.GetSection("Role").GetValue<string>("HakAkses");
        public static string ConsumeAPI = AppSettingJSON.GetSection("Host").GetValue<string>("Consume");
        public static string ConsumeAPI_ = AppSettingJSON["Host:Consume"].ToString() == string.Empty ? string.Empty : AppSettingJSONDevelopment["Host:Consume"].ToString();

        private readonly ILogger<DashboardController> _logger;
        private readonly ApplicationDBContext DBContext;

        public DashboardController(ILogger<DashboardController> logger, ApplicationDBContext DBContext)
        {
            this.DBContext = DBContext;
            _logger = logger;
        }
        #endregion

        #region Property Consume API
        /// <summary>
        /// Property Consume API ------------------------------------------------------------
        /// </summary>
        public static dynamic Result;
        public static dynamic API_Response;
		public static List<dynamic>[] Null;
		public static HttpResponseMessage HttpRequest;
        public static HttpClient HttpClient;
        /// <summary>
        /// Models Response API -------------------------------------------------------------
        /// </summary>
        public static ResponseGetAllMasterMenu ResponseGetAllData;
        public static ResponseAddSingleDataMasterMenu ResponseAddSingleData;
        public static ResponseAddMultipleDataMasterMenu ResponseAddMultipleData;
        public static ResponseUpdateSingleDataMasterMenu ResponseUpdateSingleData;
        public static ResponseUpdateMultipleDataMasterMenu ResponseUpdateMultipleData;
        public static ResponseDeleteSingleDataMasterMenu ResponseDeleteSingleData;
        public static ResponseDeleteMultipleDataMasterMenu ResponseDeleteMultipleData;
        public static ResponseGetSingleDataMasterMenu ResponseGetSingleData;
        public static ResponseGetMultipleDataMasterMenu ResponseGetMultipleData;
        #endregion

        #region Ver 1.0 : LoadData
        //[HttpPost]
        //public async Task<IActionResult> LoadData()
        //{
        //    try
        //    {
        //        var ConnectionString = AppSettingJSON.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
        //        var DBContext = new DbContextOptionsBuilder<ApplicationDBContext>().UseSqlServer(ConnectionString).Options;
        //        var Draw = Request.Form["Draw"].FirstOrDefault(); ///Request.Query
        //        var Start = Request.Form["Start"].FirstOrDefault(); ///Request.Query   
        //        var Length = Request.Form["Length"].FirstOrDefault(); ///Request.Query
        //        var SortColumn = Request.Form["Columns[" + Request.Form["Order[0][Column]"].FirstOrDefault() + "][Name]"].FirstOrDefault(); ///Request.Query
        //        var SortColumnDirection = Request.Form["Order[0][Dir]"].FirstOrDefault(); ///Request.Query
        //        var SearchValue = Request.Form["Search[Value]"].FirstOrDefault(); ///Request.Query
        //        int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
        //        int Skip = Start != null ? Convert.ToInt32(Start) : 0;
        //        int RecordsTotal = 0;
        //        var GetData = DBContext.MasterMenus.AsNoTracking().Distinct(); //.AsQueryable();
        //        var RecordsFiltered = GetData.Count();
        //        RecordsTotal = GetData.Count();
        //        if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDirection)))
        //        {
        //            GetData = GetData.OrderBy(SortColumn + " " + SortColumnDirection);
        //        }
        //        if (!string.IsNullOrEmpty(SearchValue))
        //        {
        //            GetData = GetData.Where
        //            (
        //                Object =>
        //                Object.Id.ToString().Contains(SearchValue) ||
        //                Object.KodeMenu.ToString().Contains(SearchValue) ||
        //                Object.NamaMenu.ToString().Contains(SearchValue) ||
        //                Object.JenisMenu.ToString().Contains(SearchValue) ||
        //                Object.HargaSatuan.ToString().Contains(SearchValue)
        //            );
        //        }
        //        var Data = GetData.Skip(Skip).Take(PageSize).ToList();
        //        var jsonData = new
        //        {
        //            Draw = Draw,
        //            RecordsTotal = RecordsTotal,
        //            RecordsFiltered = RecordsFiltered,
        //            Data = Data
        //        };
        //        return Ok(jsonData);
        //    }
        //    catch (Exception Error)
        //    {
        //        Console.WriteLine(Error.Message.ToString());
        //        throw;
        //    }
        //    finally
        //    {
        //    }
        //}
        #endregion

        #region Ver 1.1 : LoadData
        [HttpPost, Produces("application/json")]
        public async Task<JsonResult> LoadData()
        {
            try
            {
                HttpClient = new HttpClient();
                HttpClient.Timeout = TimeSpan.FromHours(24);
                HttpClient.BaseAddress = new Uri(ConsumeAPI + "MasterMenu/GetAllDataMasterMenu");
                HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequest = await HttpClient.GetAsync(HttpClient.BaseAddress);
                if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
                {
                    API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
                    ResponseGetAllData = JsonConvert.DeserializeObject<ResponseGetAllMasterMenu>(API_Response);

                    var Skip = DataTablesRequest?.Start;
                    var Take = DataTablesRequest?.Length;
                    var SortColumnName = DataTablesRequest?.Columns.ElementAt(DataTablesRequest.Order.ElementAt(0).Column).Name;
                    var SortDirection = DataTablesRequest?.Order.ElementAt(0).Dir.ToLower();
                    var SearchValue = DataTablesRequest?.Search?.Value?.ToUpper();
                    var Data = ResponseGetAllData.data.Distinct().Select(x => new
                    {
                        Id = x.Id == 0 ? 0 : x.Id,
                        KodeMenu = x.KodeMenu == null ? string.Empty : x.KodeMenu,
                        NamaMenu = x.NamaMenu == null ? string.Empty : x.NamaMenu,
                        JenisMenu = x.JenisMenu == null ? string.Empty : x.JenisMenu,
                        HargaSatuan = x.HargaSatuan == 0 ? string.Empty : "Rp. " + string.Format("{0:#,##0}", x.HargaSatuan) //"Rp. " + x.hargaSatuan
                    }).OrderBy(x => x.Id).AsQueryable();
                    var RecordsTotal = Data.Count();
                    var RecordsFiltered = Data.Count();
                    if (!string.IsNullOrWhiteSpace(SearchValue))
                    {
                        Data = Data.Where
                        (
                            Object =>
                            Object.Id.ToString().ToUpper().Contains(SearchValue) ||
                            Object.KodeMenu.ToUpper().Contains(SearchValue) ||
                            Object.NamaMenu.ToUpper().Contains(SearchValue) ||
                            Object.JenisMenu.ToUpper().Contains(SearchValue) ||
                            Object.HargaSatuan.ToString().ToUpper().Contains(SearchValue)
                        );
                    }
                    //Data = Data.OrderBy($"{SortColumnName} {SortDirection}");
                    return new JsonResult(new
                    {
                        Draw = DataTablesRequest?.Draw,
                        RecordsTotal = RecordsTotal,
                        RecordsFiltered = RecordsFiltered,
                        Data = Data.Skip(Convert.ToInt32(Skip)).Take(Convert.ToInt32(Take))
                    });
                }
                else
                {
                    Result = new
                    {
                        StatusCode = HttpRequest.StatusCode,
                        Message = HttpRequest.ReasonPhrase,
                        Data = Null
                    };
                    return StatusCode(500, Result);
                }
            }
            catch (ApplicationException ErrorApplicationException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = Null
                };
                Console.WriteLine(ErrorApplicationException.Message);
                return StatusCode(500, Result);
                throw new ApplicationException(ErrorApplicationException.Message);
            }
            catch (Exception ErrorException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = Null
                };
                Console.WriteLine(ErrorException.Message);
                return StatusCode(500, Result);
                throw new Exception(ErrorException.Message);
            }
            finally
            {
            }
        }
        #endregion

        [HttpPost, Produces("application/json")]
        public async Task<JsonResult> GetDataById(long Id)
        {
            try
            {
                HttpClient = new HttpClient();
                HttpClient.Timeout = TimeSpan.FromHours(24);
                HttpClient.BaseAddress = new Uri(ConsumeAPI + "MasterMenu/GetDataById/" + Id);
                HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequest = await HttpClient.PostAsJsonAsync(HttpClient.BaseAddress, Id);
                if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
                {
                    API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
                    ResponseGetSingleData = JsonConvert.DeserializeObject<ResponseGetSingleDataMasterMenu>(API_Response);
                    var GetData = await (from x in DBContext.MasterMenus.AsNoTracking() where x.Id == ResponseGetSingleData.data.Id select x).Distinct().FirstOrDefaultAsync();
                    var JSONData = new
                    {
                        Id = GetData?.Id == null || GetData?.Id.ToString() == "" || GetData?.Id.ToString() == string.Empty ? null : GetData?.Id,
                        JenisMenu = GetData?.JenisMenu == null || GetData?.JenisMenu == "" || GetData?.JenisMenu == string.Empty ? null : GetData?.JenisMenu,
                        KodeMenu = GetData?.KodeMenu == null || GetData?.KodeMenu == "" || GetData?.KodeMenu == string.Empty ? null : GetData?.KodeMenu,
                        NamaMenu = GetData?.NamaMenu == null || GetData?.NamaMenu == "" || GetData?.NamaMenu == string.Empty ? null : GetData?.NamaMenu,
                        HargaSatuan = string.Concat("Rp. ", string.Format("{0:#,##0}", GetData?.HargaSatuan) == null || GetData?.HargaSatuan == 0 ? null : string.Format("{0:#,##0}", GetData?.HargaSatuan))  //{0:#,##0.00}
                    };
                    return new JsonResult(JSONData);
                }
                else
                {
                    Result = new
                    {
                        StatusCode = HttpRequest.StatusCode,
                        Message = HttpRequest.ReasonPhrase,
                        Data = Null
                    };
                    return StatusCode(500, Result);
                }
            }
            catch (ApplicationException ErrorApplicationException)
            {
                Result = new
                {
					StatusCode = 500,
					Message = ErrorApplicationException.Message,
                    Data = Null
				};
                Console.WriteLine(ErrorApplicationException.Message);
                return StatusCode(500, Result);
				throw new ApplicationException(ErrorApplicationException.Message);
			}
            catch (Exception ErrorException)
            {
                Result = new
                {
					StatusCode = 500,
					Message = ErrorException.Message,
                    Data = Null
				};
                Console.WriteLine(ErrorException.Message);
                return StatusCode(500, Result);
                throw new Exception(ErrorException.Message);
            }
            finally
            {
            }
        }

        [HttpPost, Produces("application/json")]
        public async Task<IActionResult> CustomeSearch(string Search)
        {
            try
            {
                #region Consume API GetAllDataMasterMenu : api/MasterMenu/List
                API_Response = new HttpClient();
                HttpClient.Timeout = TimeSpan.FromHours(24);
                HttpClient.BaseAddress = new Uri(ConsumeAPI + "MasterMenu/GetAllDataMasterMenu");
                HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequest = await HttpClient.GetAsync(HttpClient.BaseAddress);
                if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
                {
                    API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
                    ResponseGetAllData = JsonConvert.DeserializeObject<ResponseGetAllMasterMenu>(API_Response);

                    var Skip = DataTablesRequest?.Start;
                    var Take = DataTablesRequest?.Length;
                    var SortColumnName = DataTablesRequest?.Columns.ElementAt(DataTablesRequest.Order.ElementAt(0).Column).Name;
                    var SortDirection = DataTablesRequest?.Order.ElementAt(0).Dir.ToLower();
                    var SearchValue = DataTablesRequest?.Search?.Value?.ToUpper();
                    var Data = ResponseGetAllData.data.Distinct().Select(x => new
                    {
                        Id = x.Id == 0 ? 0 : x.Id,
                        KodeMenu = x.KodeMenu == null ? string.Empty : x.KodeMenu,
                        NamaMenu = x.NamaMenu == null ? string.Empty : x.NamaMenu,
                        JenisMenu = x.JenisMenu == null ? string.Empty : x.JenisMenu,
                        HargaSatuan = x.HargaSatuan == 0 ? string.Empty : "Rp. " + string.Format("{0:#,##0}", x.HargaSatuan) //"Rp. " + x.hargaSatuan
                    }).OrderBy(x => x.Id).Skip(Convert.ToInt32(Skip)).Take(Convert.ToInt32(Take)).AsQueryable();
                    var RecordsTotal = Data.Count();
                    var RecordsFiltered = Data.Count();
                    if (!string.IsNullOrWhiteSpace(SearchValue))
                    {
                        Data = Data.Where
                        (
                            Object =>
                            Object.Id.ToString().ToUpper().Contains(SearchValue) ||
                            Object.KodeMenu.ToUpper().Contains(SearchValue) ||
                            Object.NamaMenu.ToUpper().Contains(SearchValue) ||
                            Object.JenisMenu.ToUpper().Contains(SearchValue) ||
                            Object.HargaSatuan.ToString().ToUpper().Contains(SearchValue)
                        );
                    }
                    //Data = Data.OrderBy($"{SortColumnName} {SortDirection}");
                    return new JsonResult(new
                    {
                        Draw = DataTablesRequest?.Draw,
                        RecordsTotal = RecordsTotal,
                        RecordsFiltered = RecordsFiltered,
                        Data = Data
                    });
                }
                else
                {
                    Result = new
                    {
                        StatusCode = HttpRequest.StatusCode,
                        Message = HttpRequest.ReasonPhrase,
                        Data = Null
                    };
                    return StatusCode(500, Result);
                }
                #endregion
            }
            catch (ApplicationException ErrorApplicationException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = Null
                };
                Console.WriteLine(ErrorApplicationException.Message);
                return StatusCode(500, Result);
				throw new ApplicationException(ErrorApplicationException.Message);
			}
            catch (Exception ErrorException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = Null
				};
                Console.WriteLine(ErrorException.Message);
                return StatusCode(500, Result);
                throw new Exception(ErrorException.Message);
            }
            finally
            {
            }
        }

        public IActionResult Dashboard()
        {
            //TempData["Username"] = HttpContext.Session.Id;
            return View();
        }

        public IActionResult MenuPesanan()
        {
            return new RedirectResult(url: "/MasterPesanan/MasterPesanan", permanent: true, preserveMethod: true);
            //Response.Redirect("/MasterPesanan/MasterPesanan");
            //return View();
        }

        public IActionResult MenuTransaksi()
        {
            return new RedirectResult(url: "/MasterTransaksi/MasterTransaksi", permanent: true, preserveMethod: true);
            //Response.Redirect("/MasterTransaksi/MasterTransaksi");
            //return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
