﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Data;
using System.Linq.Dynamic.Core;
using My_API.Additional;
using My_API.Connection;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net;
using static My_API.Additional.ResponseAPI.ResponsePesanan;
using static My_API.Additional.ResponseAPI.ResponseDaftarPemesan;
using Microsoft.Extensions.Hosting;
using static My_API.Additional.ResponseAPI;

namespace My_Portfolio.Controllers
{
    public class MasterPesananController : Controller
    {
        #region Property DataTables
        [BindProperty]
        public DataTables.DataTablesRequest? DataTablesRequest { get; set; }
		#endregion

		#region Property Connection Database
		static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
		static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
		public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
		public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
		public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;
		public static string RoleHakAkses = AppSettingJSON.GetSection("Role").GetValue<string>("HakAkses");
		public static string ConsumeAPI = AppSettingJSON.GetSection("Host").GetValue<string>("Consume");
		public static string ConsumeAPI_ = AppSettingJSON["Host:Consume"].ToString() == string.Empty ? string.Empty : AppSettingJSONDevelopment["Host:Consume"].ToString();

		private readonly ILogger<MasterPesananController> _logger;
        private readonly ApplicationDBContext DBContext;

        public MasterPesananController(ILogger<MasterPesananController> logger, ApplicationDBContext DBContext)
        {
            this.DBContext = DBContext;
            _logger = logger;
        }
		#endregion

		#region Property Consume API
		/// <summary>
		/// Property Consume API ------------------------------------------------------------
		/// </summary>
		public static dynamic Result;
		public static dynamic API_Response;
		public static List<dynamic>[] Null;
		public static HttpResponseMessage HttpRequest;
        public static HttpClient HttpClient;
		/// <summary>
		/// Models Response API -------------------------------------------------------------
		/// </summary>
		//-----------------------------------------------------------------------------------
		public static ResponseMenuPesanan ResponseMenuPesanan;
		//-----------------------------------------------------------------------------------
		public static ResponseGetAllPesanan ResponseGetAllData;
        public static ResponseAddSingleDataPesanan ResponseAddSingleData;
        public static ResponseAddMultipleDataPesanan ResponseAddMultipleData;
        public static ResponseUpdateSingleDataPesanan ResponseUpdateSingleData;
        public static ResponseUpdateMultipleDataPesanan ResponseUpdateMultipleData;
        public static ResponseDeleteSingleDataPesanan ResponseDeleteSingleData;
        public static ResponseDeleteMultipleDataPesanan ResponseDeleteMultipleData;
        public static ResponseGetSingleDataPesanan ResponseGetSingleData;
        public static ResponseGetMultipleDataPesanan ResponseGetMultipleData;
		#endregion

		#region Ver 1.0 : LoadData
		//[HttpPost]
		//public async Task<IActionResult> LoadData()
		//{
		//    try
		//    {
		//        //var AppDBContext = new DbContextOptionsBuilder<ApplicationDBContext>().UseSqlServer(@"Server=RESHAPROGRAMMER; Database=DB_Inspirotechs; User Id=sa; Password=P@ssw0rd; Trusted_Connection=True; TrustServerCertificate=True; MultipleActiveResultSets=true; PersistSecurityInfo=True;").Options;
		//        //var DBContext = new ApplicationDBContext(AppDBContext);
		//        var DBContext = new ApplicationDBContext(GetConnection);
		//        var Draw = Request.Form["Draw"].FirstOrDefault(); ///Request.Query
		//        var Start = Request.Form["Start"].FirstOrDefault(); ///Request.Query   
		//        var Length = Request.Form["Length"].FirstOrDefault(); ///Request.Query
		//        var SortColumn = Request.Form["Columns[" + Request.Form["Order[0][Column]"].FirstOrDefault() + "][Name]"].FirstOrDefault(); ///Request.Query
		//        var SortColumnDirection = Request.Form["Order[0][Dir]"].FirstOrDefault(); ///Request.Query
		//        var SearchValue = Request.Form["Search[Value]"].FirstOrDefault(); ///Request.Query
		//        int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
		//        int Skip = Start != null ? Convert.ToInt32(Start) : 0;
		//        int RecordsTotal = 0;
		//        var MasterPesananQuery = DBContext.MasterPesanans.AsNoTracking().AsQueryable();
		//        var RecordsFiltered = MasterPesananQuery.Count();
		//        RecordsTotal = MasterPesananQuery.Count();
		//        if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDirection)))
		//        {
		//            MasterPesananQuery = MasterPesananQuery.OrderBy(SortColumn + " " + SortColumnDirection);
		//        }
		//        if (!string.IsNullOrEmpty(SearchValue))
		//        {
		//            MasterPesananQuery = MasterPesananQuery.Where
		//            (
		//                Object =>
		//                Object.Id.ToString().ToUpper().Contains(SearchValue) ||
		//                Object.KodePemesanan.ToString().ToUpper().Contains(SearchValue) ||
		//                Object.NamaPemesan.ToUpper().Contains(SearchValue) ||
		//                Object.KodeMenu.ToString().ToUpper().Contains(SearchValue) ||
		//                Object.NamaMenu.ToUpper().Contains(SearchValue) ||
		//                Object.JumlahPesanan.ToString().ToUpper().Contains(SearchValue) ||
		//              Object.HargaSatuan.ToString().Contains(SearchValue)
		//            );
		//        }
		//        var Data = MasterPesananQuery.Skip(Skip).Take(PageSize).ToList();
		//        var jsonData = new
		//        {
		//            Draw = Draw,
		//            RecordsTotal = RecordsTotal,
		//            RecordsFiltered = RecordsFiltered,
		//            Data = Data
		//        };
		//        return Ok(jsonData);
		//    }
		//    catch (Exception Error)
		//    {
		//        Console.WriteLine(Error.Message.ToString());
		//        throw;
		//    }
		//    finally
		//    {
		//    }
		//}
		#endregion

		#region Ver 1.1 : LoadData
		[HttpPost, Produces("application/json")]
		public async Task<JsonResult> LoadData()
		{
			try
			{
				HttpClient = new HttpClient();
				HttpClient.Timeout = TimeSpan.FromHours(24);
				HttpClient.BaseAddress = new Uri(ConsumeAPI + "Pesanan/GetAllDataJoin_DaftarPemesan_Pesanan");
				HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				HttpRequest = await HttpClient.GetAsync(HttpClient.BaseAddress);
				if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
				{
					API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
					ResponseMenuPesanan = JsonConvert.DeserializeObject<ResponseMenuPesanan>(API_Response);
					var Skip = DataTablesRequest?.Start;
					var Take = DataTablesRequest?.Length;
					var SortColumnName = DataTablesRequest?.Columns.ElementAt(DataTablesRequest.Order.ElementAt(0).Column).Name;
					var SortDirection = DataTablesRequest?.Order.ElementAt(0).Dir.ToLower();
					var SearchValue = DataTablesRequest?.Search?.Value?.ToUpper();
					var Data = ResponseMenuPesanan.data.Select(x => new 
					{
						Id = x.Id == 0 ? 0 : x.Id,
						KodeTransaksi = x.KodeTransaksi == null ? string.Empty : x.KodeTransaksi,
						KodePemesan = x.KodePemesan == null ? string.Empty : x.KodePemesan,
						NamaPemesan = x.NamaPemesan == null ? string.Empty : x.NamaPemesan,
						KodeMenu = x.KodeMenu == null ? string.Empty : x.KodeMenu,
						NamaMenu = x.NamaMenu == null ? string.Empty : x.NamaMenu,
						JumlahPesanan = x.JumlahPesanan == 0 ? 0 : x.JumlahPesanan,
						HargaSatuan = x.HargaSatuan == null ? string.Empty : x.HargaSatuan
                    }).Distinct().OrderBy(x => x.Id).AsQueryable();
					var RecordsTotal = Data.Count();
					var RecordsFiltered = Data.Count();
					if (!string.IsNullOrWhiteSpace(SearchValue))
					{
						Data = Data.Where
						(
							Object =>
							Object.Id.ToString().ToUpper().Contains(SearchValue) ||
							Object.KodeTransaksi.ToUpper().Contains(SearchValue) ||
							Object.KodePemesan.ToUpper().Contains(SearchValue) ||
							Object.NamaPemesan.ToUpper().Contains(SearchValue) ||
							Object.KodeMenu.ToUpper().Contains(SearchValue) ||
							Object.NamaMenu.ToUpper().Contains(SearchValue) ||
							Object.JumlahPesanan.ToString().ToUpper().Contains(SearchValue) ||
							Object.HargaSatuan.ToString().ToUpper().Contains(SearchValue)
						);
					}
					//Data = Data.OrderBy($"{SortColumnName} {SortDirection}");
					return new JsonResult(new
					{
						Draw = DataTablesRequest?.Draw,
						RecordsTotal = RecordsTotal,
						RecordsFiltered = RecordsFiltered,
						Data = Data.Skip(Convert.ToInt32(Skip)).Take(Convert.ToInt32(Take))
					});
				}
				else
				{
					Result = new
					{
						StatusCode = HttpRequest.StatusCode,
						Message = HttpRequest.ReasonPhrase,
						Data = Null
					};
					return StatusCode(500, Result);
				}
			}
			catch (ApplicationException ErrorApplicationException)
			{
				Result = new
				{
					StatusCode = HttpRequest.StatusCode,
					Message = HttpRequest.ReasonPhrase,
					Data = Null
				};
				Console.WriteLine(ErrorApplicationException.Message);
				return StatusCode(500, Result);
				throw new ApplicationException(ErrorApplicationException.Message);
			}
			catch (Exception ErrorException)
			{
				Result = new
				{
					StatusCode = HttpRequest.StatusCode,
					Message = HttpRequest.ReasonPhrase,
					Data = Null
				};
				Console.WriteLine(ErrorException.Message);
				return StatusCode(500, Result);
				throw new Exception(ErrorException.Message);
			}
			finally
			{
			}
		}
		#endregion

		[HttpPost, Produces("application/json")]
		public async Task<IActionResult> AutonumberKodePesanan(string data)
		{
			try
			{
				int Auto = 1;
				int TemporaryNumber;
				var Kode = string.Empty;
				var Result = string.Empty;
				var NamaMenu = string.Empty;
				if (data == "Food" || data.Contains("Food"))
				{
					var QueryMasterPesanan = await (from MasterMenu in DBContext.MasterMenus
													where MasterMenu.JenisMenu == data || MasterMenu.JenisMenu.Contains(data)
													orderby (int?)MasterMenu.KodeMenu.Length descending, MasterMenu.KodeMenu descending
													select new
													{
														MasterMenu.Id,
														MasterMenu.JenisMenu,
														MasterMenu.KodeMenu,
														MasterMenu.NamaMenu
													}).Take(1).ToListAsync();

					foreach (var Obj in QueryMasterPesanan)
					{
						TemporaryNumber = Convert.ToInt32(Obj.KodeMenu.Substring(Obj.KodeMenu.Length - 2, 2)) + Auto;
						Kode = Obj.KodeMenu.Substring(0, 7);
						Result = string.Concat(Kode, TemporaryNumber);
						NamaMenu = Obj.NamaMenu;
					}
				}
				else if (data == "Drink" || data.Contains("Drink"))
				{
					var QueryMasterPesanan = await (from MasterMenu in DBContext.MasterMenus
													where MasterMenu.JenisMenu == data || MasterMenu.JenisMenu.Contains(data)
													orderby (int?)MasterMenu.KodeMenu.Length descending, MasterMenu.KodeMenu descending
													select new
													{
														MasterMenu.Id,
														MasterMenu.JenisMenu,
														MasterMenu.KodeMenu,
														MasterMenu.NamaMenu
													}).Take(1).ToListAsync();

					foreach (var Obj in QueryMasterPesanan)
					{
						TemporaryNumber = Convert.ToInt32(Obj.KodeMenu.Substring(Obj.KodeMenu.Length - 1, 1)) + Auto;
						Kode = Obj.KodeMenu.Substring(0, 7);
						Result = string.Concat(Kode, TemporaryNumber);
						NamaMenu = Obj.NamaMenu;
					}
				}

				var JSONData = new
				{
					Result
				};
				//return Ok(JSONData);
				return View(JSONData);
			}
			catch (ApplicationException ErrorApplicationException)
			{
				Result = new
				{
					StatusCode = 500,
					Message = ErrorApplicationException.Message,
					Data = Null
				};
				Console.WriteLine(ErrorApplicationException.Message);
				return StatusCode(500, Result);
				throw new ApplicationException(ErrorApplicationException.Message);
			}
			catch (Exception ErrorException)
			{
				Result = new
				{
					StatusCode = 500,
					Message = ErrorException.Message,
					Data = Null
				};
				Console.WriteLine(ErrorException.Message);
				return StatusCode(500, Result);
				throw new Exception(ErrorException.Message);
			}
			finally
			{
			}
		}

        public IActionResult MasterPesanan()
        {
            return View();
        }

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}