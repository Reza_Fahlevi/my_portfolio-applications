﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using My_API.Additional;
using My_API.Connection;
using My_API.Models;
using Newtonsoft.Json;
using System.Data;
using System.Diagnostics;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Net.Http.Headers;
using static My_API.Additional.ResponseAPI.ResponseMasterMenu;

namespace My_Portfolio.Controllers
{
    public class MasterMenuController : Controller
    {
        #region Property DataTables
        [BindProperty]
        public DataTables.DataTablesRequest? DataTablesRequest { get; set; }
        #endregion

        #region Property Connection Database
        static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
        static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
        public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;
        public static string RoleHakAkses = AppSettingJSON.GetSection("Role").GetValue<string>("HakAkses");
        public static string ConsumeAPI = AppSettingJSON.GetSection("Host").GetValue<string>("Consume");
        public static string ConsumeAPI_ = AppSettingJSON["Host:Consume"].ToString() == string.Empty ? string.Empty : AppSettingJSONDevelopment["Host:Consume"].ToString();

        private readonly ILogger<MasterMenuController> _logger;
        private readonly ApplicationDBContext DBContext;

        public MasterMenuController(ILogger<MasterMenuController> logger, ApplicationDBContext DBContext)
        {
            this.DBContext = DBContext;
            _logger = logger;
        }
        #endregion

        #region Property Consume API
        /// <summary>
        /// Property Consume API ------------------------------------------------------------
        /// </summary>
        public static dynamic Result;
        public static dynamic API_Response;
        public static List<dynamic>[] Null;
        public static HttpResponseMessage HttpRequest;
        public static HttpClient HttpClient;
        /// <summary>
        /// Models Response API -------------------------------------------------------------
        /// </summary>
        public static ResponseGetAllMasterMenu ResponseGetAllData;
        public static ResponseAddSingleDataMasterMenu ResponseAddSingleData;
        public static ResponseAddMultipleDataMasterMenu ResponseAddMultipleData;
        public static ResponseUpdateSingleDataMasterMenu ResponseUpdateSingleData;
        public static ResponseUpdateMultipleDataMasterMenu ResponseUpdateMultipleData;
        public static ResponseDeleteSingleDataMasterMenu ResponseDeleteSingleData;
        public static ResponseDeleteMultipleDataMasterMenu ResponseDeleteMultipleData;
        public static ResponseGetSingleDataMasterMenu ResponseGetSingleData;
        public static ResponseGetMultipleDataMasterMenu ResponseGetMultipleData;
        #endregion

        #region Ver 1.0 : LoadData
        //[HttpPost]
        //public async Task<IActionResult> LoadData()
        //{
        //    try
        //    {
        //        var ConnectionString = AppSettingJSON.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
        //        var DBContext = new DbContextOptionsBuilder<ApplicationDBContext>().UseSqlServer(ConnectionString).Options;
        //        var DBContext = new ApplicationDBContext(GetConnection);
        //        var Draw = Request.Form["Draw"].FirstOrDefault(); ///Request.Query
        //        var Start = Request.Form["Start"].FirstOrDefault(); ///Request.Query   
        //        var Length = Request.Form["Length"].FirstOrDefault(); ///Request.Query
        //        var SortColumn = Request.Form["Columns[" + Request.Form["Order[0][Column]"].FirstOrDefault() + "][Name]"].FirstOrDefault(); ///Request.Query
        //        var SortColumnDirection = Request.Form["Order[0][Dir]"].FirstOrDefault(); ///Request.Query
        //        var SearchValue = Request.Form["Search[Value]"].FirstOrDefault(); ///Request.Query
        //        int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
        //        int Skip = Start != null ? Convert.ToInt32(Start) : 0;
        //        int RecordsTotal = 0;
        //        var MasterMenuQuery = DBContext.MasterMenus.AsNoTracking().AsQueryable();
        //        var RecordsFiltered = MasterMenuQuery.Count();
        //        RecordsTotal = MasterMenuQuery.Count();
        //        if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDirection)))
        //        {
        //            MasterMenuQuery = MasterMenuQuery.OrderBy(SortColumn + " " + SortColumnDirection);
        //        }
        //        if (!string.IsNullOrEmpty(SearchValue))
        //        {
        //            MasterMenuQuery = MasterMenuQuery.Where
        //            (
        //                Object =>
        //                Object.Id.ToString().ToUpper().Contains(SearchValue) ||
        //                Object.KodeMenu.ToUpper().Contains(SearchValue) ||
        //                Object.NamaMenu.ToUpper().Contains(SearchValue) ||
        //                Object.JenisMenu.ToUpper().Contains(SearchValue) ||
        //                Object.HargaSatuan.ToString().ToUpper().Contains(SearchValue)
        //            );
        //        }
        //        var Data = MasterMenuQuery.Skip(Skip).Take(PageSize).ToList();
        //        var jsonData = new
        //        {
        //            Draw = Draw,
        //            RecordsTotal = RecordsTotal,
        //            RecordsFiltered = RecordsFiltered,
        //            Data = Data
        //        };
        //        return Ok(jsonData);
        //    }
        //    catch (Exception Error)
        //    {
        //        Console.WriteLine(Error.Message.ToString());
        //        throw;
        //    }
        //    finally
        //    {
        //    }
        //}
        #endregion

        #region Ver 1.1 : LoadData
        [HttpPost, Produces("application/json")]
        public async Task<JsonResult> LoadData()
        {
            try
            {
                HttpClient = new HttpClient();
                HttpClient.Timeout = TimeSpan.FromHours(24);
                HttpClient.BaseAddress = new Uri(ConsumeAPI + "MasterMenu/GetAllDataMasterMenu");
                HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequest = await HttpClient.GetAsync(HttpClient.BaseAddress);
                if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
                {
                    API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
                    ResponseGetAllData = JsonConvert.DeserializeObject<ResponseGetAllMasterMenu>(API_Response);

                    var Skip = DataTablesRequest?.Start;
                    var Take = DataTablesRequest?.Length;
                    var SortColumnName = DataTablesRequest?.Columns.ElementAt(DataTablesRequest.Order.ElementAt(0).Column).Name;
                    var SortDirection = DataTablesRequest?.Order.ElementAt(0).Dir.ToLower();
                    var SearchValue = DataTablesRequest?.Search?.Value?.ToUpper();
                    var Data = ResponseGetAllData.data.Distinct().Select(x => new
                    {
                        Id = x.Id == 0 ? 0 : x.Id,
                        KodeMenu = x.KodeMenu == null ? string.Empty : x.KodeMenu,
                        NamaMenu = x.NamaMenu == null ? string.Empty : x.NamaMenu,
                        JenisMenu = x.JenisMenu == null ? string.Empty : x.JenisMenu,
                        HargaSatuan = x.HargaSatuan == 0 ? string.Empty : "Rp. " + string.Format("{0:#,##0}", x.HargaSatuan) //"Rp. " + x.hargaSatuan
                    }).OrderBy(x => x.Id).AsQueryable();
                    var RecordsTotal = Data.Count();
                    var RecordsFiltered = Data.Count();
                    if (!string.IsNullOrWhiteSpace(SearchValue))
                    {
                        Data = Data.Where
                        (
                            Object =>
                            Object.Id.ToString().ToUpper().Contains(SearchValue) ||
                            Object.KodeMenu.ToUpper().Contains(SearchValue) ||
                            Object.NamaMenu.ToUpper().Contains(SearchValue) ||
                            Object.JenisMenu.ToUpper().Contains(SearchValue) ||
                            Object.HargaSatuan.ToString().ToUpper().Contains(SearchValue)
                        );
                    }
                    //Data = Data.OrderBy($"{SortColumnName} {SortDirection}");
                    return new JsonResult(new
                    {
                        Draw = DataTablesRequest?.Draw,
                        RecordsTotal = RecordsTotal,
                        RecordsFiltered = RecordsFiltered,
                        Data = Data.Skip(Convert.ToInt32(Skip)).Take(Convert.ToInt32(Take))
                    });
                }
                else
                {
                    Result = new
                    {
                        StatusCode = HttpRequest.StatusCode,
                        Message = HttpRequest.ReasonPhrase,
                        Data = 0
                    };
                    return StatusCode(500, Result);
                }
            }
            catch (ApplicationException ErrorApplicationException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = 0
                };
                Console.WriteLine(ErrorApplicationException.Message);
                return StatusCode(500, Result);
                throw new ApplicationException(ErrorApplicationException.Message);
            }
            catch (Exception ErrorException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = 0
                };
                Console.WriteLine(ErrorException.Message);
                return StatusCode(500, Result);
                throw new Exception(ErrorException.Message);
            }
            finally
            {
            }
        }
        #endregion

        #region Autonumber KodeMenu
        [HttpPost, Produces("application/json")]
        public async Task<IActionResult> Autonumber_KodeMenu([FromBody] string JenisMenu)
        {
            try
            {
                int Auto = 1;
                dynamic CodeUnique = null;
                dynamic CodeNumber = null;
                dynamic GetData = null;
                int AutoDigitNumber;
                var kodeMenu = string.Empty;
                if (JenisMenu == "Food" || JenisMenu.Contains("Food"))
                {
                    GetData = await (from Food in DBContext.MasterMenus.AsNoTracking()
                                    where Food.JenisMenu == JenisMenu || Food.JenisMenu.Contains(JenisMenu)
                                    orderby (int?)Food.KodeMenu.Length descending, Food.KodeMenu descending
									select new
                                    {
                                        Food.KodeMenu
                                    }).FirstOrDefaultAsync(); 

                    CodeUnique = GetData?.KodeMenu == null || GetData?.KodeMenu == "" || GetData?.KodeMenu == string.Empty ? null : GetData?.KodeMenu.Substring(0, 1);
                    CodeNumber = GetData?.KodeMenu == null || GetData?.KodeMenu == "" || GetData?.KodeMenu == string.Empty ? null : GetData?.KodeMenu.Substring(1);

                    AutoDigitNumber = Convert.ToInt32(CodeNumber) + Auto;
                    kodeMenu = string.Concat(CodeUnique, AutoDigitNumber);
                }
                else if (JenisMenu == "Drink" || JenisMenu.Contains("Drink"))
                {
                    GetData = await (from Drink in DBContext.MasterMenus.AsNoTracking()
                                    where Drink.JenisMenu == JenisMenu || Drink.JenisMenu.Contains(JenisMenu)
                                    orderby (int?)Drink.KodeMenu.Length descending, Drink.KodeMenu descending
                                    select new
                                    {
                                        Drink.KodeMenu
                                    }).FirstOrDefaultAsync(); 

                    CodeUnique = GetData?.KodeMenu == null || GetData?.KodeMenu == "" || GetData?.KodeMenu == string.Empty ? null : GetData?.KodeMenu.Substring(0, 1);
                    CodeNumber = GetData?.KodeMenu == null || GetData?.KodeMenu == "" || GetData?.KodeMenu == string.Empty ? null : GetData?.KodeMenu.Substring(1);

                    AutoDigitNumber = Convert.ToInt32(CodeNumber) + Auto;
                    kodeMenu = string.Concat(CodeUnique, AutoDigitNumber);
                }

                var JSONData = new
                {
                    kodeMenu
                };
                //return Ok(JSONData);
                //return new JsonResult(JSONData);
                return await Task.FromResult(Json(JSONData));
            }
			catch (ApplicationException ErrorApplicationException)
			{
				Result = new
				{
					StatusCode = 500,
					Message = ErrorApplicationException.Message,
					Data = Null
				};
				Console.WriteLine(ErrorApplicationException.Message);
				return StatusCode(500, Result);
				throw new ApplicationException(ErrorApplicationException.Message);
			}
			catch (Exception ErrorException)
			{
				Result = new
				{
					StatusCode = 500,
					Message = ErrorException.Message,
					Data = Null
				};
				Console.WriteLine(ErrorException.Message);
				return StatusCode(500, Result);
				throw new Exception(ErrorException.Message);
			}
			finally
			{
			}
		}
        #endregion

        #region Add Data
        [HttpPost, Produces("application/json")]
        public async Task<IActionResult> AddData([FromBody] PayloadAddDataMasterMenu Payload)
        {
            try
            {
                HttpClient = new HttpClient();
                HttpClient.Timeout = TimeSpan.FromHours(24);
                HttpClient.BaseAddress = new Uri(ConsumeAPI + "MasterMenu/AddData");
                HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequest = new HttpResponseMessage();
                HttpRequest = await HttpClient.PostAsJsonAsync(HttpClient.BaseAddress, Payload);
                if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
                {
                    API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
                    ResponseAddSingleData = JsonConvert.DeserializeObject<ResponseAddSingleDataMasterMenu>(API_Response);
                    ///----------------------------------------------------------------------------------------------------
                    //Result = new
                    //{
                    //    StatusCode = 200,
                    //    Message = ResponseAddSingleData.message,
                    //    Data = ResponseAddSingleData.data
                    //};
                    //return StatusCode(200, Result);
                    ///----------------------------------------------------------------------------------------------------
                    //return new RedirectResult(url: "/MasterMenu/MasterMenu", permanent: true, preserveMethod: true);
                    //return View();
                    //return this.RedirectToAction("MasterMenu", "MasterMenu");
                    ///----------------------------------------------------------------------------------------------------
                    return Redirect("/MasterMenu/MasterMenu");
                }
                else
                {
                    ///----------------------------------------------------------------------------------------------------
                    //Result = new
                    //{
                    //    StatusCode = HttpRequest.StatusCode,
                    //    Message = HttpRequest.ReasonPhrase,
                    //    Data = ResponseAddSingleData.data
                    //};
                    //return StatusCode(500, Result);
                    ///----------------------------------------------------------------------------------------------------
                    //return new RedirectResult(url: "/MasterMenu/MasterMenu", permanent: true, preserveMethod: true);
                    //return View();
                    //return this.RedirectToAction("MasterMenu", "MasterMenu");
                    return Redirect("/MasterMenu/MasterMenu");
                    ///----------------------------------------------------------------------------------------------------
                }
            }
            catch (ApplicationException ErrorApplicationException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = ResponseAddSingleData.data
                };
                Console.WriteLine(ErrorApplicationException.Message);
                return StatusCode(500, Result);
				throw new ApplicationException(ErrorApplicationException.Message);
			}
            catch (Exception ErrorException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = ResponseAddSingleData.data
                };
                Console.WriteLine(ErrorException.Message);
                return StatusCode(500, Result);
                throw new Exception(ErrorException.Message);
            }
            finally
            {
            }
        }
        #endregion

        #region Update Data
        [HttpPost, Produces("application/json")]
        public async Task<IActionResult> UpdateData([FromBody] PayloadUpdateDataMasterMenu Payload)
        {
            try
            {
                HttpClient = new HttpClient();
                HttpClient.Timeout = TimeSpan.FromHours(24);
                HttpClient.BaseAddress = new Uri(ConsumeAPI + "MasterMenu/UpdateData");
                HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequest = await HttpClient.PostAsJsonAsync(HttpClient.BaseAddress, Payload);
                if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
                {
                    API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
                    ResponseUpdateSingleData = JsonConvert.DeserializeObject<ResponseUpdateSingleDataMasterMenu>(API_Response);
                    ///----------------------------------------------------------------------------------------------------
                    //Result = new
                    //{
                    //    StatusCode = HttpRequest.StatusCode,
                    //    Message = ResponseUpdateSingleData.message,
                    //    Data = ResponseUpdateSingleData.data
                    //};
                    //return StatusCode(200, Result);
                    ///----------------------------------------------------------------------------------------------------
                    //Response.Redirect("/MasterMenu/MasterMenu");
                    //return View("/MasterMenu/MasterMenu");
                    return new RedirectResult(url: "/MasterMenu/MasterMenu", permanent: true, preserveMethod: true);
                    ///----------------------------------------------------------------------------------------------------
                }
                else
                {
                    ///----------------------------------------------------------------------------------------------------
                    //Result = new
                    //{
                    //    StatusCode = HttpRequest.StatusCode,
                    //    Message = ResponseUpdateSingleData.message,
                    //    Data = ResponseUpdateSingleData.data
                    //};
                    //return StatusCode(200, Result);
                    ///----------------------------------------------------------------------------------------------------
                                        //Response.Redirect("/MasterMenu/MasterMenu");
                    //return View("/MasterMenu/MasterMenu");
                    return new RedirectResult(url: "/MasterMenu/MasterMenu", permanent: true, preserveMethod: true);
                    ///----------------------------------------------------------------------------------------------------
                }
            }
            catch (ApplicationException ErrorApplicationException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = ResponseUpdateSingleData.data
                };
                Console.WriteLine(ErrorApplicationException.Message);
                return StatusCode(500, Result);
				throw new ApplicationException(ErrorApplicationException.Message);
			}
            catch (Exception ErrorException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = ResponseUpdateSingleData.data
                };
                Console.WriteLine(ErrorException.Message);
                return StatusCode(500, Result);
                throw new Exception(ErrorException.Message);
            }
            finally
            {
            }
        }
        #endregion

        #region Delete Data
        [HttpDelete, Produces("application/json")]
        public async Task<JsonResult> DeleteDataById([FromBody] string kodeMenu)
        {
            try
            {
                var Data = await (from x in DBContext.MasterMenus.AsNoTracking().Include(x => x.DaftarPemesans).ThenInclude(x => x.KodeMenuNavigation) where x.KodeMenu == kodeMenu select x).Distinct().FirstOrDefaultAsync();
                DBContext.Database.BeginTransaction();
                DBContext.MasterMenus.RemoveRange(Data);
                DBContext.Database.CommitTransaction();
                DBContext.SaveChanges();
                return new JsonResult(1);
            }
            catch (ApplicationException ErrorApplicationException)
            {
                DBContext.Database.RollbackTransaction();
                Result = new
                {
                    StatusCode = 500,
                    Message = ErrorApplicationException.Message,
                    Data = 0
                };
                Console.WriteLine(ErrorApplicationException.Message);
                return StatusCode(500, Result);
				throw new ApplicationException(ErrorApplicationException.Message);
			}
            catch (Exception ErrorException)
            {
                DBContext.Database.RollbackTransaction();
                Result = new
                {
                    StatusCode = 500,
                    Message = ErrorException.Message,
                    Data = 0
                };
                Console.WriteLine(ErrorException.Message);
                return StatusCode(500, Result);
                throw new Exception(ErrorException.Message);
            }
            finally
            {
            }
        }
        #endregion

        #region Get Data
        [HttpPost, Produces("application/json")]
        public async Task<IActionResult> GetDataById([FromBody] long Id)
        {
            try
            {
                HttpClient = new HttpClient();
                HttpClient.Timeout = TimeSpan.FromHours(24);
                HttpClient.BaseAddress = new Uri(ConsumeAPI + "MasterMenu/GetDataById/" + Id);
                HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequest = await HttpClient.PostAsJsonAsync(HttpClient.BaseAddress, Id);
                if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
                {
                    API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
                    ResponseGetSingleData = JsonConvert.DeserializeObject<ResponseGetSingleDataMasterMenu>(API_Response);
                    var GetData = await (from x in DBContext.MasterMenus.AsNoTracking() where x.Id == ResponseGetSingleData.data.Id select x).Distinct().FirstOrDefaultAsync();
                    var JSONData = new
                    {
                        Id = GetData?.Id == null || GetData?.Id.ToString() == "" || GetData?.Id.ToString() == string.Empty ? null : GetData?.Id,
                        JenisMenu = GetData?.JenisMenu == null || GetData?.JenisMenu == "" || GetData?.JenisMenu == string.Empty ? null : GetData?.JenisMenu,
                        KodeMenu = GetData?.KodeMenu == null || GetData?.KodeMenu == "" || GetData?.KodeMenu == string.Empty ? null : GetData?.KodeMenu,
                        NamaMenu = GetData?.NamaMenu == null || GetData?.NamaMenu == "" || GetData?.NamaMenu == string.Empty ? null : GetData?.NamaMenu,
                        HargaSatuan = string.Concat("Rp. ", string.Format("{0:#,##0}", GetData?.HargaSatuan) == null || GetData?.HargaSatuan == 0 ? null : string.Format("{0:#,##0}", GetData?.HargaSatuan))  //{0:#,##0.00}
                    };
                    return new JsonResult(JSONData);
                }
                else
                {
                    Result = new
                    {
                        StatusCode = HttpRequest.StatusCode,
                        Message = HttpRequest.ReasonPhrase,
                        Data = Null
                    };
                    return StatusCode(500, Result);
                }
            }
            catch (ApplicationException ErrorApplicationException)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = ErrorApplicationException.Message,
                    Data = 0
                };
                Console.WriteLine(ErrorApplicationException.Message);
                return StatusCode(500, Result);
				throw new ApplicationException(ErrorApplicationException.Message);
			}
            catch (Exception ErrorException)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = ErrorException.Message,
                    Data = 0
                };
                Console.WriteLine(ErrorException.Message);
                return StatusCode(500, Result);
                throw new Exception(ErrorException.Message);
            }
            finally
            {
            }
        }
        #endregion

        public IActionResult MasterMenu()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Example Include And ThenInclude
        //public async Task<IActionResult> Example_Include_ThenInclude()
        //{
        //    ///Penggunaan "AsSingleQuery()" akan melakukan 1 kali hit select table, tetapi akan terjadi double data pada table tersebut.
        //    ///Penggunaan "AsNoTracking()" hanya untuk select data <--- jangan dipakai untuk update, delete, insert data.
        //    ///Penggunaan "AsSplitQuery()" data tidak akan double, tetapi akan terjadi 3 kali select table A, B, C atau lebih dari jumlah banyaknya table.
        //    ///
        //    var Test1 = await applicationDBContext.MasterMenus.ToListAsync(); ///<--- default'nya menggunakan AsSingleQuery()
        //    var Test2 = await applicationDBContext.MasterMenus.Include(x => x.MasterPesanans).AsNoTracking().ToListAsync();
        //    var Test3 = await applicationDBContext.MasterMenus.Include(x => x.MasterPesanans).ThenInclude(x => x.MasterTransaksis).AsNoTracking().AsSplitQuery().Select(a => new 
        //    { 
        //        KodeMenu = a.KodeMenu,
        //        NamaMenu = a.NamaMenu,
        //        MasterPesanans = a.MasterPesanans.Select(b => new 
        //        {
        //            KodePemesanan = b.KodePemesanan,
        //            NamaPemesan = b.NamaPemesan,
        //            MasterTransaksis = b.MasterTransaksis.Select(c => new 
        //            {
        //                KodeMenu = c.KodeMenu,
        //                EmployeeId = c.EmployeeId,
        //                PesananId = c.PesananId
        //            })
        //        })
        //    }).ToListAsync();
        //    return Json(Test3);
        //}
        #endregion
    }
}
