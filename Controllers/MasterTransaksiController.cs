﻿using Microsoft.AspNetCore.Mvc;
using My_API.Additional;
using My_API.Connection;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Net;
using static My_API.Additional.ResponseAPI.ResponseTransaksi;

namespace My_Portfolio.Controllers
{
    public class MasterTransaksiController : Controller
    {
        #region Property DataTables
        [BindProperty]
        public DataTables.DataTablesRequest? DataTablesRequest { get; set; }
		#endregion

		#region Property Connection Database
		static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
		static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
		public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
		public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
		public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;
		public static string RoleHakAkses = AppSettingJSON.GetSection("Role").GetValue<string>("HakAkses");
		public static string ConsumeAPI = AppSettingJSON.GetSection("Host").GetValue<string>("Consume");
		public static string ConsumeAPI_ = AppSettingJSON["Host:Consume"].ToString() == string.Empty ? string.Empty : AppSettingJSONDevelopment["Host:Consume"].ToString();

		private readonly ILogger<MasterTransaksiController> _logger;
        private readonly ApplicationDBContext DBContext;

        public MasterTransaksiController(ILogger<MasterTransaksiController> logger, ApplicationDBContext DBContext)
        {
            this.DBContext = DBContext;
            _logger = logger;
        }
		#endregion

		#region Property Consume API
		/// <summary>
		/// Property Consume API ------------------------------------------------------------
		/// </summary>
		public static dynamic Result;
        public static dynamic API_Response;
        public static List<dynamic>[] Null;
        public static HttpResponseMessage HttpRequest;
        public static HttpClient HttpClient;
		/// <summary>
		/// Models Response API -------------------------------------------------------------
		/// </summary>
		public static ResponseGetAllTransaksi ResponseGetAllData;
        public static ResponseAddSingleDataTransaksi ResponseAddSingleData;
        public static ResponseAddMultipleDataTransaksi ResponseAddMultipleData;
        public static ResponseUpdateSingleDataTransaksi ResponseUpdateSingleData;
        public static ResponseUpdateMultipleDataTransaksi ResponseUpdateMultipleData;
        public static ResponseDeleteSingleDataTransaksi ResponseDeleteSingleData;
        public static ResponseDeleteMultipleDataTransaksi ResponseDeleteMultipleData;
        public static ResponseGetSingleDataTransaksi ResponseGetSingleData;
        public static ResponseGetMultipleDataTransaksi ResponseGetMultipleData;
		#endregion

        #region Ver 1.1 : LoadData
        [HttpPost, Produces("application/json")]
        public async Task<JsonResult> LoadData()
        {
            try
            {
                HttpClient = new HttpClient();
                HttpClient.Timeout = TimeSpan.FromHours(24);
                HttpClient.BaseAddress = new Uri(ConsumeAPI + "Transaksi/GetAllDataTransaksi");
                HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequest = await HttpClient.GetAsync(HttpClient.BaseAddress);
                if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
                {
                    API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
                    ResponseGetAllData = JsonConvert.DeserializeObject<ResponseGetAllTransaksi>(API_Response);

                    var Skip = DataTablesRequest?.Start;
                    var Take = DataTablesRequest?.Length;
                    var SortColumnName = DataTablesRequest?.Columns.ElementAt(DataTablesRequest.Order.ElementAt(0).Column).Name;
                    var SortDirection = DataTablesRequest?.Order.ElementAt(0).Dir.ToLower();
                    var SearchValue = DataTablesRequest?.Search?.Value?.ToUpper();
                    var Data = ResponseGetAllData.data.Distinct().Select(x => new
                    {
                        Id = x.Id == 0 ? 0 : x.Id,
                        KodeTransaksi = x.KodeTransaksi == null ? string.Empty : x.KodeTransaksi,
                        TotalPembayaran = x.TotalPembayaran == 0 ? string.Empty : "Rp. " + string.Format("{0:#,##0}", x.TotalPembayaran), //"Rp. " + x.TotalPembayaran
                        StatusPembayaran = x.StatusPembayaran == true ? true : x.StatusPembayaran,
                        TanggalPembayaran = x.TanggalPembayaran,
                    }).OrderBy(x => x.Id).AsQueryable();
                    var RecordsTotal = Data.Count();
                    var RecordsFiltered = Data.Count();
                    if (!string.IsNullOrWhiteSpace(SearchValue))
                    {
                        Data = Data.Where
                        (
                            Object =>
                            Object.Id.ToString().ToUpper().Contains(SearchValue) ||
                            Object.KodeTransaksi.ToUpper().Contains(SearchValue) ||
                            Object.TotalPembayaran.ToUpper().Contains(SearchValue) ||
                            Object.StatusPembayaran.ToString().Contains(SearchValue) ||
                            Object.TanggalPembayaran.ToString().ToUpper().Contains(SearchValue)
                        );
                    }
                    //Data = Data.OrderBy($"{SortColumnName} {SortDirection}");
                    return new JsonResult(new
                    {
                        Draw = DataTablesRequest?.Draw,
                        RecordsTotal = RecordsTotal,
                        RecordsFiltered = RecordsFiltered,
                        Data = Data.Skip(Convert.ToInt32(Skip)).Take(Convert.ToInt32(Take))
                    });
                }
                else
                {
                    Result = new
                    {
                        StatusCode = HttpRequest.StatusCode,
                        Message = HttpRequest.ReasonPhrase,
                        Data = 0
                    };
                    return StatusCode(500, Result);
                }
            }
            catch (ApplicationException ErrorApplicationException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = 0
                };
                Console.WriteLine(ErrorApplicationException.Message);
                return StatusCode(500, Result);
                throw new ApplicationException(ErrorApplicationException.Message);
            }
            catch (Exception ErrorException)
            {
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = 0
                };
                Console.WriteLine(ErrorException.Message);
                return StatusCode(500, Result);
                throw new Exception(ErrorException.Message);
            }
            finally
            {
            }
        }
		#endregion

		public IActionResult MasterTransaksi()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
