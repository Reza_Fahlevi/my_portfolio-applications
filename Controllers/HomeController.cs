﻿using Microsoft.AspNetCore.Mvc;
using System.Linq.Dynamic.Core;
using System.Diagnostics;
using System.Net.Http.Headers;
using My_API.Connection;
using My_API.Additional;
using My_API.Models;
using System.Net;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using HistorySignIn = My_API.Models.HistorySignIn;
using static My_API.Additional.ResponseAPI;
using static My_API.Additional.ResponseAPI.ResponseMasterEmployee;

namespace My_Portfolio.Controllers
{
    public class HomeController : Controller
    {
        #region Property Connection Database
        static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
        static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
        public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;
        public static string RoleHakAkses = AppSettingJSON.GetSection("Role").GetValue<string>("HakAkses");
        public static string ConsumeAPI = AppSettingJSON.GetSection("Host").GetValue<string>("Consume");
        public static string ConsumeAPI_ = AppSettingJSON["Host:Consume"].ToString() == string.Empty ? string.Empty : AppSettingJSONDevelopment["Host:Consume"].ToString();

        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDBContext DBContext;

        public HomeController(ILogger<HomeController> logger, ApplicationDBContext DBContext)
        {
            this.DBContext = DBContext;
            _logger = logger;
        }
        #endregion

        #region Property Consume API
        /// <summary>
        /// Property Consume API ------------------------------------------------------------
        /// </summary>
        public static dynamic Result;
        public static dynamic API_Response;
        public static List<dynamic>[] Null;
        public static HttpResponseMessage HttpRequest;
        public static HttpClient HttpClient;
        /// <summary>
        /// Models Response API -------------------------------------------------------------
        /// </summary>
        public static ResponseGetSingleDataMasterEmployee ResponseGetSingleData;
        public static ResponseAddSingleDataHistorySignIn ResponseAddSingleData;
        #endregion

        [HttpPost, Produces("application/json")]
        public async Task<JsonResult> Login([FromBody] PayloadLogin Payload)
        {
			var ResponseLogin = new ResponseLogin();
            var PayloadHistorySignIn = new HistorySignIn();
            try
            {
                HttpClient = new HttpClient();
                HttpClient.Timeout = TimeSpan.FromHours(24);
                HttpClient.BaseAddress = new Uri(ConsumeAPI + "MasterEmployee/GetDataByUsernameLogin/" + Payload.Username);
                HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequest = new HttpResponseMessage();
                HttpRequest = await HttpClient.GetAsync(HttpClient.BaseAddress);
                if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
                {
                    API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
                    ResponseGetSingleData = JsonConvert.DeserializeObject<ResponseGetSingleDataMasterEmployee>(API_Response);
                    ResponseLogin.Message = "Status " + HttpStatusCode.OK + " And Data " + Convert.ToString(HttpStatusCode.Found);
                    ResponseLogin.GetStatus = 1;
                    ResponseLogin.UserName = ResponseGetSingleData.data.UsernameLogin;
                    TempData["Username"] = ResponseGetSingleData.data.UsernameLogin;

                    if (ResponseGetSingleData.data.UsernameLogin == Payload.Username && ResponseGetSingleData.data.PasswordLogin == Payload.Password && ResponseGetSingleData.data.HakAkses == RoleHakAkses && ResponseGetSingleData.data.IsActive == true)
                    {
                        PayloadHistorySignIn.Nik = ResponseGetSingleData.data.Nik;
                        PayloadHistorySignIn.LastLogged = DateTime.Now;

                        HttpClient = new HttpClient();
                        HttpClient.Timeout = TimeSpan.FromHours(24);
                        HttpClient.BaseAddress = new Uri(ConsumeAPI + "HistorySignIn/AddData");
                        HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        HttpRequest = new HttpResponseMessage();
                        HttpRequest = await HttpClient.PostAsJsonAsync(HttpClient.BaseAddress, PayloadHistorySignIn);
                        if (HttpRequest.IsSuccessStatusCode == true || HttpRequest.StatusCode == HttpStatusCode.OK)
                        {
                            API_Response = HttpRequest.Content.ReadAsStringAsync().Result;
                            ResponseAddSingleData = JsonConvert.DeserializeObject<ResponseAddSingleDataHistorySignIn>(API_Response);
                            ResponseLogin.Message = "Status " + HttpStatusCode.OK + " And Data " + Convert.ToString(HttpStatusCode.Found);
                            ResponseLogin.GetStatus = 1;
                            ResponseLogin.UserName = ResponseGetSingleData.data.UsernameLogin;
                            TempData["Username"] = ResponseGetSingleData.data.UsernameLogin;
                        }
                        else
                        {
                            ResponseLogin.Message = "Status " + HttpStatusCode.InternalServerError + " And Data " + Convert.ToString(HttpStatusCode.NotFound);
                            ResponseLogin.GetStatus = 0;
                            ResponseLogin.UserName = string.Empty;
                            RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        ResponseLogin.Message = "Status " + HttpStatusCode.InternalServerError + " And Data " + Convert.ToString(HttpStatusCode.NotFound);
                        ResponseLogin.GetStatus = 0;
                        ResponseLogin.UserName = string.Empty;
                        RedirectToAction("Index");
                    }
                }
                else
                {
                    ResponseLogin.Message = "Status " + HttpStatusCode.InternalServerError + " And Data " + Convert.ToString(HttpStatusCode.NotFound);
                    ResponseLogin.GetStatus = 0;
                    ResponseLogin.UserName = string.Empty;
                    RedirectToAction("Index");
                }
                return new JsonResult(ResponseLogin);
			}
			catch (ApplicationException ErrorApplicationException)
			{
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = new 
                    {
						GetStatus = ResponseLogin.GetStatus,
						UserName = ResponseLogin.UserName
                    }
                };
                Console.WriteLine(ErrorApplicationException.Message);
                return StatusCode(500, Result);
				throw new ApplicationException(ErrorApplicationException.Message);
			}
			catch (Exception ErrorException)
			{
                Result = new
                {
                    StatusCode = HttpRequest.StatusCode,
                    Message = HttpRequest.ReasonPhrase,
                    Data = new
                    {
                        GetStatus = ResponseLogin.GetStatus,
                        UserName = ResponseLogin.UserName
                    }
                };
                Console.WriteLine(ErrorException.Message);
                return StatusCode(500, Result);
                throw new Exception(ErrorException.Message);
			}
			finally
			{
			}
		}

        public IActionResult Home()
        {
            return View();
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}