﻿$(document).ready(function () {
    var DT = $("#DataTablesPesanan").DataTable({
        //"scrollY": 100,
        //"scrollX": true,
        //"scrollCollapse": true,
        //"scrollResize": true,
        //"responsive": true,
        //"processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": true, // for disable multiple column at once
        "fixedHeader": true, // this is fixed header
        "pagingType": "full_numbers", // this is for paging type
        "lengthChange": true, // this is for length change
        "lengthMenu":  // this is for length dropdown
        [
            [5, 10, 25, 50, 100, 1000],
            [5, 10, 25, 50, 100, 1000],
        ],
        "ajax":
        {
            "url": "/MasterPesanan/LoadData",
            "type": "POST",
            "datatype": "JSON"
        },
        "columnDefs":
        [{
            "targets": [0],
            "visible": true,
            "searchable": true,
            "className": "SelectedAllCheckbox",
            "checkboxes":
            {
                "selectRow": true,
                "selectAllPages": false
            },
        }],
        "select":
        {
            "style": "multi",
            "selector": "td:first-child"
        },
        "order": [[1, "asc"]],
        "columns":
        [
            //{
            //    "targets": 0,
            //    "data": null,
            //    "className": "SelectedAllCheckbox",
            //    "searchable": false,
            //    "orderable": false,
            //    "autoWidth": false,
            //    "render": function (data, row, type, full, meta) {
            //        return '<input type="checkbox" class="SelectedAllCheckbox" name="HeaderAllCheckbox" id="HeaderAllCheckbox" ClientIDMode="Static" value="' + data.id + '" onclick=GetById("' + data.id + '")>';
            //        ////return "<input type='checkbox class='SelectedAllCheckbox' name='HeaderAllCheckbox' id='HeaderAllCheckbox' ClientIDMode='Static' onclick=GetById('" + data.id + "')";
            //    },
            //},
            {
                "data": "id",
                "name": "id",
                "autoWidth": true, "target": 0, "visible": true, "searchable": true, "orderable": true
            },
            {
                "data": "kodeTransaksi",
                "name": "kodeTransaksi",
                "autoWidth": true, "target": 1, "visible": true, "searchable": true, "orderable": true
            },
            {
                "data": "kodePemesan",
                "name": "kodePemesan",
                "autoWidth": true, "target": 2, "visible": true, "searchable": true, "orderable": true
            },
            {
                "data": "namaPemesan",
                "name": "namaPemesan",
                "autoWidth": true, "target": 3, "visible": true, "searchable": true, "orderable": true
            },
            {
                "data": "jenisPesanan",
                "name": "jenisPesanan",
                "autoWidth": true, "target": 4, "visible": true, "searchable": true, "orderable": true
            },
            {
                "data": "kodeMenu",
                "name": "kodeMenu",
                "autoWidth": true, "target": 5, "visible": true, "searchable": true, "orderable": true
            },
            {
                "data": "namaMenu",
                "name": "namaMenu",
                "autoWidth": true, "target": 6, "visible": true, "searchable": true, "orderable": true
            },
            {
                "data": "jumlahPesanan",
                "name": "jumlahPesanan",
                "autoWidth": true, "target": 7, "visible": true, "searchable": true, "orderable": true
            },
            {
                "data": "hargaSatuan",
                "name": "hargaSatuan",
                "autoWidth": true, "target": 8, "visible": true, "searchable": true, "orderable": true
            },
            {
                "data": null,
                "name": null,
                "autoWidth": true, "target": 9, "visible": true, "searchable": false, "orderable": false, "render": function (data, row, type, full, meta) {
                    return "<a href='#PopUpDetailItemPesanan' data-toggle='modal' data-target='#PopUpDetailItemPesanan' id='ButtonGetById' ClientIDMode='Static' class='css-button-custome_details' onclick='GetDetailPesananById(\"" + data.kodePemesan + "\",\"" + data.namaPemesan + "\",\"" + data.jenisPesanan + "\",\"" + data.kodeMenu + "\",\"" + data.namaMenu + "\",\"" + data.jumlahPesanan + "\",\"" + data.hargaSatuan + "\");'><span class='css-button-custome_details-icon'><i class='fa fa-eye'></i></span><span class='css-button-custome_details-text'><span>Details</span></span></a>";
                }
            }
        ]
    });

    /*Benar------------------------------------------*/
    $('#HeaderAllCheckbox').click(function (e) {
        if ($(this).hasClass('SelectedAllCheckbox')) {
            $('input').prop('checked', false);
            $(this).removeClass('SelectedAllCheckbox');
        }
        else {
            $('input').prop('checked', true);
            $(this).addClass('SelectedAllCheckbox');
        }
    });
    /*----------------------------------------------*/
});


function GetDetailPesananById(kodePemesan, namaPemesan, jenisPesanan, kodeMenu, namaMenu, jumlahPesanan, hargaSatuan) {
    /*------------------------------------------------------------------------*/

    //alert("Menu yang anda pilih adalah Id nomor : " + id);
    console.log("KodePemesan : " + kodePemesan, "NamaPemesan : " + namaPemesan, "Jenis Pesanan : " + jenisPesanan, "KodeMenu : " + kodeMenu, "NamaMenu : " + namaMenu, "JumlahPesanan : " + jumlahPesanan, "Rp. " + hargaSatuan);
    $("#PopUpDetailItemPesanan").modal('show')
    //$("#PopUp_TextboxId").val(id);
    $("#PopUp_TextboxKodePemesan").val(kodePemesan);
    $("#PopUp_TextboxNamaPemesan").val(namaPemesan);
    $("#PopUp_TextboxJenisPesanan").val(jenisPesanan);
    $("#PopUp_TextboxKodeMenu").val(kodeMenu);
    $("#PopUp_TextboxNamaMenu").val(namaMenu);
    $("#PopUp_TextboxJumlahPesanan").val(jumlahPesanan);
    $("#PopUp_TextboxHargaSatuan").val(hargaSatuan);

    /*------------------------------------------------------------------------*/
}


//function GetById(id) {
//    alert("Menu yang anda pilih adalah Id nomor : " + id);
//    console.log(id);
//}


function DeleteData(id) {
    if (confirm("Are you sure you want to delete ...?")) {
        Delete(id);
        console.log(id);
    }
    else {
        return false;
    }
}


function Delete(id) {
    var url = "/MasterPesanan/DeleteData";
    $.post(url, { ID: id }, function (data) {
        if (data) {
            oTable = $('#DataTablesPesanan').DataTable();
            oTable.draw();
        }
        else {
            alert("Something Went Wrong!");
        }
    });
}

function ClosePopUpModal() {
    $("#PopUpDetailItemPesanan").modal('hide')
}